package com.LastHackerGames.WobbleRocket.GameWorld;

import java.util.Stack;

import com.LastHackerGames.WobbleRocket.IActivityRequestHandler;
import com.LastHackerGames.WobbleRocket.GameObjects.Missile;
import com.LastHackerGames.WobbleRocket.GameObjects.ScrollHandler;
import com.LastHackerGames.WobbleRocket.GameObjects.WobblyMissile;
import com.LastHackerGames.WobbleRocket.Helpers.AssetLoader;
import com.LastHackerGames.WobbleRocket.ui.Button;

public class GameWorld {

	/**
	 * 204 x 136
	 * 
	 */

	public enum GameState {
		READY, RUNNING, GAMEOVER, HIGHSCORE, UNLOCKED, RESTART
	}

	public enum MissileTypes {
		NORMAL(0), ORANGE(5), GOLD(25), GREEN(50), RED(100), RAINBOW(200);

		private final int id;

		MissileTypes(int id) {
			this.id = id;
		}

		public int getValue() {
			return id;
		}
	}

	private GameState currentState;
	private float runTime = 0;
	private Stack<Integer> unlocked;
	private ScrollHandler scroller;
	private Missile missile, missileDemo;
	private Button retry, share, play, rate, ok;
	private int score = 0;
	private IActivityRequestHandler myRequestHandler;

	public GameWorld(int midpoint, float width, float height,
			IActivityRequestHandler myRequestHandler) {
		this.myRequestHandler = myRequestHandler;
		this.currentState = GameState.READY;
		this.missile = new WobblyMissile(width / 2, midpoint - 75, 10, 47);
		this.missileDemo = new WobblyMissile(width / 2, midpoint - 23, 10, 47);
		this.scroller = new ScrollHandler(this, midpoint + 66);
		this.unlocked = new Stack<Integer>();

		// Share | Retry
		float buttonHeight = 188;
		float gap = 7;
		float sizeMult = 0.5f;
		this.share = new Button(width / 2 - gap
				- (AssetLoader.share.getRegionWidth() * sizeMult),
				buttonHeight, (AssetLoader.share.getRegionWidth() * sizeMult),
				(AssetLoader.share.getRegionHeight() * sizeMult),
				AssetLoader.share, AssetLoader.sharePressed);
		this.retry = new Button(width / 2 + gap, buttonHeight,
				(AssetLoader.retry.getRegionWidth() * sizeMult),
				(AssetLoader.retry.getRegionHeight() * sizeMult),
				AssetLoader.retry, AssetLoader.retryPressed);

		// Rate | Play
		this.rate = new Button(width / 2 - gap
				- (AssetLoader.rate.getRegionWidth() * sizeMult), buttonHeight,
				(AssetLoader.rate.getRegionWidth() * sizeMult),
				(AssetLoader.rate.getRegionHeight() * sizeMult),
				AssetLoader.rate, AssetLoader.ratePressed);
		this.play = new Button(width / 2 + gap, buttonHeight,
				(AssetLoader.play.getRegionWidth() * sizeMult),
				(AssetLoader.play.getRegionHeight() * sizeMult),
				AssetLoader.play, AssetLoader.playPressed);

		// OK
		this.ok = new Button(width / 2
				- (AssetLoader.ok.getRegionWidth() / 2 * sizeMult),
				buttonHeight, (AssetLoader.ok.getRegionWidth() * sizeMult),
				(AssetLoader.ok.getRegionHeight() * sizeMult), AssetLoader.ok,
				AssetLoader.okPressed);

	}

	public void update(float delta) {
		runTime += delta;

		switch (currentState) {
		case READY:
			updateReady(delta);
			break;

		case RUNNING:
			updateRunning(delta);
			break;

		case UNLOCKED:
			updateReadyDemo(delta);
			break;
			
		case RESTART:
			start();
			break;
			
		default:
			break;
		}

	}

	private void updateReady(float delta) {
		missile.updateReady(runTime);
		scroller.updateReady(delta);
	}

	private void updateReadyDemo(float delta) {
		missileDemo.updateReady(runTime);
		scroller.updateReady(delta);
	}

	public void updateRunning(float delta) {

		if (delta > .15f) {
			delta = .15f;
		}
		missile.update(delta);
		scroller.update(delta);

		if (scroller.collides(missile) && missile.isAlive()) {
			gameOver();
			if (score > AssetLoader.getHighScore()) {
		
				if (unlock(score, AssetLoader.getHighScore()) > 0) {
					unlocked();
				} else {
					highscore();
				}

				AssetLoader.setHighScore(score);

			} else {
				AssetLoader.gameOverSound.play();
			}

		}

	}

	private int unlock(int score, int prevScore) {

		if (score >= GameWorld.MissileTypes.RAINBOW.getValue()
				&& prevScore < GameWorld.MissileTypes.RAINBOW.getValue()) {
			unlocked.push(GameWorld.MissileTypes.RAINBOW.getValue());
		}
		if (score >= GameWorld.MissileTypes.GOLD.getValue()
				&& prevScore < GameWorld.MissileTypes.GOLD.getValue()) {
			unlocked.push(GameWorld.MissileTypes.GOLD.getValue());
		}
		if (score >= GameWorld.MissileTypes.RED.getValue()
				&& prevScore < GameWorld.MissileTypes.RED.getValue()) {
			unlocked.push(GameWorld.MissileTypes.RED.getValue());
		}
		if (score >= GameWorld.MissileTypes.GREEN.getValue()
				&& prevScore < GameWorld.MissileTypes.GREEN.getValue()) {
			unlocked.push(GameWorld.MissileTypes.GREEN.getValue());
		}
		if (score >= GameWorld.MissileTypes.ORANGE.getValue()
				&& prevScore < GameWorld.MissileTypes.ORANGE.getValue()) {
			unlocked.push(GameWorld.MissileTypes.ORANGE.getValue());
		}
		return unlocked.size();
	}

	public void start() {
		myRequestHandler.showAds(false);
		
		currentState = GameState.RUNNING;
	}

	public void restart() {
		myRequestHandler.showAds(true);
		score = 0;
		missile.onRestart();
		scroller.onRestart();
		currentState = GameState.RESTART;		
	}

	public void gameOver() {
		myRequestHandler.showAds(true);
		scroller.stop();
		missile.die();
		AssetLoader.deadSound.play();
		currentState = GameState.GAMEOVER;
	}

	public void ready() {
		myRequestHandler.showAds(true);
		currentState = GameState.READY;
	}

	public void unlock() {
		myRequestHandler.showAds(true);
	}

	public void highscore() {
		myRequestHandler.showAds(true);
		AssetLoader.highscoreSound.play();
		currentState = GameState.HIGHSCORE;
	}

	public boolean isGameOver() {
		return currentState == GameState.GAMEOVER;
	}

	public boolean isGameUnlocked() {
		return currentState == GameState.UNLOCKED;
	}

	public boolean isHighScore() {
		return currentState == GameState.HIGHSCORE;
	}

	public boolean isReady() {
		return currentState == GameState.READY;
	}

	public boolean isRunning() {
		return currentState == GameState.RUNNING;
	}

	public Missile getMissile() {
		return this.missile;
	}

	public void addScore(int increment) {
		score += increment;
	}

	public Button getRetryButton() {
		return retry;
	}

	public Button getShareButton() {
		return share;
	}

	public Button getPlayButton() {
		return play;
	}

	public Button getRateButton() {
		return rate;
	}

	public ScrollHandler getScroller() {
		// TODO Auto-generated method stub
		return scroller;
	}

	public int getScore() {
		// TODO Auto-generated method stub
		return score;
	}

	public Button getOkButton() {
		// TODO Auto-generated method stub
		return ok;
	}

	public boolean isUnlocked() {
		return currentState == GameState.UNLOCKED;
	}

	public Integer nextUnlock() {
		if (unlocked.size() <= 1)
			currentState = GameState.HIGHSCORE;
		return unlocked.pop();
	}

	public Integer getUnlocked() {
		return unlocked.peek();
	}

	public Missile getMissileDemo() {
		// TODO Auto-generated method stub
		return missileDemo;
	}

	public void unlocked() {
		AssetLoader.unlockedSound.play();
		currentState = GameState.UNLOCKED;
	}

}
