package com.LastHackerGames.WobbleRocket.GameWorld;

import com.LastHackerGames.WobbleRocket.GameObjects.Edge;
import com.LastHackerGames.WobbleRocket.GameObjects.Missile;
import com.LastHackerGames.WobbleRocket.GameObjects.ScrollHandler;
import com.LastHackerGames.WobbleRocket.GameObjects.Wall;
import com.LastHackerGames.WobbleRocket.Helpers.AssetLoader;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GameRenderer {

	private GameWorld world;
	private ShapeRenderer renderer;
	private Missile missile, missileDemo;

	private OrthographicCamera cam;

	private SpriteBatch batcher;
	private ScrollHandler scroller;
	private Wall wall1, wall2;
	private Edge bottomEdge1, bottomEdge2, topEdge1, topEdge2;
	private Texture background;

	private Animation missileAnimation, missileDemoAnimation;

	private TextureRegion bottomEdgeTexture, topEdgeTexture,
			brickLeftEndTexture, brickRightEndTexture, resultScreenTexture,
			missileOff, missileDead, arrowLeftTexture, arrowRightTexture,
			highscoreTexture, gameOverTexture, unlockedTexture, tapLeftTexture,
			tapRightTexture, checkOnTexture, checkOffTexture;

	Texture brickTexture;
	private Color skyColour;
	private float width, height;

	private float minY;
	private boolean needsLoading = false;

	public GameRenderer(GameWorld world, float width, float height) {
		this.world = world;
		this.width = width;
		this.height = height;

		cam = new OrthographicCamera();
		cam.setToOrtho(true, width, height);

		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);

		renderer = new ShapeRenderer();
		renderer.setProjectionMatrix(cam.combined);

		minY = 18;
		if (Gdx.graphics.getHeight() < 350)
			minY = 25;
		else if (Gdx.graphics.getHeight() < 420)
			minY = 22;
		else if (Gdx.graphics.getHeight() < 500)
			minY = 20;
		// (.0f*(Gdx.graphics.getHeight()/204.0f));

	
		
		initGameObjects();
		initAssets();

	}

	private void initAssets() {

		// TODO: load correct animation

		bottomEdgeTexture = AssetLoader.bottomEdge;
		topEdgeTexture = AssetLoader.topEdge;
		brickTexture = AssetLoader.brick;
		brickLeftEndTexture = AssetLoader.brickLeftEnd;
		brickRightEndTexture = AssetLoader.brickRightEnd;
		resultScreenTexture = AssetLoader.resultScreen;

		arrowLeftTexture = AssetLoader.arrowLeft;
		arrowRightTexture = AssetLoader.arrowRight;
		highscoreTexture = AssetLoader.highScore;
		gameOverTexture = AssetLoader.gameOver;
		unlockedTexture = AssetLoader.unlocked;
		tapLeftTexture = AssetLoader.tapLeft;
		tapRightTexture = AssetLoader.tapRight;
		checkOnTexture = AssetLoader.checkOn;
		checkOffTexture = AssetLoader.checkOff;
		missileOff = AssetLoader.missileOff;

		skyColour = new Color(67 / 255.0f, 145 / 255.0f, 217 / 255.0f, 1);

		background = AssetLoader.background;

		// Switch based on skill
		loadRocket(AssetLoader.getHighScore());

	}

	private void initGameObjects() {
		missile = world.getMissile();
		missileDemo = world.getMissileDemo();
		scroller = world.getScroller();
		wall1 = scroller.getwall1();
		wall2 = scroller.getwall2();
		bottomEdge1 = scroller.getBottomEdge1();
		bottomEdge2 = scroller.getBottomEdge2();
		topEdge1 = scroller.getTopEdge1();
		topEdge2 = scroller.getTopEdge2();

	}

	public void loadRocket(int score) {
		if (score >= GameWorld.MissileTypes.RAINBOW.getValue()) {
			missileAnimation = AssetLoader.missileAnimationRainbow;
			missileDead = AssetLoader.missileRainbowDead;
		} else if (score >= GameWorld.MissileTypes.RED.getValue()) {
			missileAnimation = AssetLoader.missileAnimationRed;
			missileDead = AssetLoader.missileRedDead;
		} else if (score >= GameWorld.MissileTypes.GREEN.getValue()) {
			missileAnimation = AssetLoader.missileAnimationGreen;
			missileDead = AssetLoader.missileGreenDead;
		} else if (score >= GameWorld.MissileTypes.GOLD.getValue()) {
			missileAnimation = AssetLoader.missileAnimationGold;
			missileDead = AssetLoader.missileGoldDead;
		} else if (score >= GameWorld.MissileTypes.ORANGE.getValue()) {
			missileAnimation = AssetLoader.missileAnimationOrange;
			missileDead = AssetLoader.missileOrangeDead;
		} else {
			missileAnimation = AssetLoader.missileAnimation;
			missileDead = AssetLoader.missileDead;
		}
		needsLoading = false;
	}

	private void loadDemo(int score) {
		if (score >= GameWorld.MissileTypes.RAINBOW.getValue()) {
			missileDemoAnimation = AssetLoader.missileAnimationRainbow;

		} else if (score >= GameWorld.MissileTypes.RED.getValue()) {
			missileDemoAnimation = AssetLoader.missileAnimationRed;

		} else if (score >= GameWorld.MissileTypes.GREEN.getValue()) {
			missileDemoAnimation = AssetLoader.missileAnimationGreen;

		} else if (score >= GameWorld.MissileTypes.GOLD.getValue()) {
			missileDemoAnimation = AssetLoader.missileAnimationGold;

		} else if (score >= GameWorld.MissileTypes.ORANGE.getValue()) {
			missileDemoAnimation = AssetLoader.missileAnimationOrange;

		}
	}

	private void drawMissile(float runtime) {
		if (missile.isAlive()) {
			batcher.draw(missileAnimation.getKeyFrame(runtime), missile.getX(),
					missile.getY(), missile.getWidth() / 2.0f,
					missile.getHeight() / 2.0f, missile.getWidth(),
					missile.getHeight(), 1, 1, missile.getRotation());
		} else {
			batcher.draw(missileDead, missile.getX(), missile.getY(),
					missile.getWidth() / 2.0f, missile.getHeight() / 2.0f,
					missile.getWidth(), missile.getHeight(), 1, 1,
					missile.getRotation());
		}
	}

	private void drawEdge() {
		batcher.draw(bottomEdgeTexture, bottomEdge1.getX(), bottomEdge1.getY(),
				bottomEdge1.getWidth(), bottomEdge1.getHeight());
		batcher.draw(bottomEdgeTexture, bottomEdge2.getX(), bottomEdge2.getY(),
				bottomEdge2.getWidth(), bottomEdge2.getHeight());
		batcher.draw(topEdgeTexture, topEdge1.getX(), topEdge1.getY(),
				topEdge1.getWidth(), topEdge1.getHeight());
		batcher.draw(topEdgeTexture, topEdge2.getX(), topEdge2.getY(),
				topEdge2.getWidth(), topEdge2.getHeight());
	}

	private void drawObstacles() {
		// draw darker grey for the building walls

		batcher.draw(brickTexture, wall1.getX(), wall1.getY(),
				wall1.getWidth() - 5, wall1.getHeight(), 0, 0,
				(int) wall1.getWidth() / 16, 1);
		batcher.draw(brickLeftEndTexture, wall1.getWidth() - 5, wall1.getY(),
				5, wall1.getHeight());

		batcher.draw(brickTexture, wall2.getX() + 5, wall2.getY(),
				wall2.getWidth(), wall2.getHeight(),
				(int) wall2.getWidth() / 16, 0, 0, 1);
		batcher.draw(brickRightEndTexture, wall2.getX(), wall2.getY(), 5,
				wall2.getHeight());

	}

	private void drawScore() {
		int length = ("" + world.getScore()).length();
		AssetLoader.shadow.draw(batcher, "" + world.getScore(),
				68 - (3 * length), height / 2 - 92);
		AssetLoader.font.draw(batcher, "" + world.getScore(),
				68 - (3 * length), height / 2 - 93);
	}

	private void drawGameOver() {

		// batcher.draw(resultScreenTexture, 32, 77, 74,50);
		batcher.draw(gameOverTexture, width / 2 - 55, minY, 110, 14);
	}

	private void drawHighScore() {

		// batcher.draw(resultScreenTexture, 32, 77, 74,50);
		batcher.draw(highscoreTexture, width / 2 - 56, minY, 112, 14);
	}

	private void drawUnlocked() {

		// batcher.draw(resultScreenTexture, 32, 77, 74,50);
		batcher.draw(unlockedTexture, width / 2 - 56, minY, 112, 14);
	}

	private void drawResults() {

		// batcher.draw(resultScreenTexture, 32, 77, 74,50);
		int resultScreenWidth = 88;
		float midpoint = width / 2 + 5;
		float resultScreenLeft = midpoint - (resultScreenWidth / 2);

		batcher.draw(resultScreenTexture, resultScreenLeft, 90,
				resultScreenWidth, 64);

		// draw score, highscore and rating
		int length = ("" + world.getScore()).length();
		AssetLoader.microFont.draw(batcher, "" + world.getScore(), midpoint
				- (length / 2) - (resultScreenWidth / 4), 101);

		length = ("" + AssetLoader.getHighScore()).length();
		AssetLoader.microFont.draw(batcher, "" + AssetLoader.getHighScore(),
				midpoint - (length / 2) + (resultScreenWidth / 4) - 4, 101);

		int checkY = 120;
		int checkHeight = 20;
		int checkWidth = 12;
		int score = world.getScore();

		
		if (score >= GameWorld.MissileTypes.ORANGE.getValue()) {
			batcher.draw(checkOnTexture, resultScreenLeft
					+ (resultScreenWidth * 1 / 7), checkY, checkWidth,
					checkHeight);
		} else {
			batcher.draw(checkOffTexture, resultScreenLeft
					+ (resultScreenWidth * 1 / 7), checkY, checkWidth,
					checkHeight);
		}

		
		if (score >= GameWorld.MissileTypes.GOLD.getValue()) {
			batcher.draw(checkOnTexture, resultScreenLeft
					+ (resultScreenWidth * 2 / 7), checkY, checkWidth,
					checkHeight);
		} else {
			batcher.draw(checkOffTexture, resultScreenLeft
					+ (resultScreenWidth * 2 / 7), checkY, checkWidth,
					checkHeight);
		}
		
		
		if (score >= GameWorld.MissileTypes.GREEN.getValue()) {
			batcher.draw(checkOnTexture, resultScreenLeft
					+ (resultScreenWidth * 3 / 7), checkY, checkWidth,
					checkHeight);
		} else {
			batcher.draw(checkOffTexture, resultScreenLeft
					+ (resultScreenWidth * 3 / 7), checkY, checkWidth,
					checkHeight);
		}

		if (score >= GameWorld.MissileTypes.RED.getValue()) {
			batcher.draw(checkOnTexture, resultScreenLeft
					+ (resultScreenWidth * 4 / 7), checkY, checkWidth,
					checkHeight);
		} else {
			batcher.draw(checkOffTexture, resultScreenLeft
					+ (resultScreenWidth * 4 / 7), checkY, checkWidth,
					checkHeight);
		}



		if (score >= GameWorld.MissileTypes.RAINBOW.getValue()) {
			batcher.draw(checkOnTexture, resultScreenLeft
					+ (resultScreenWidth * 5 / 7), checkY, checkWidth,
					checkHeight);
		} else {
			batcher.draw(checkOffTexture, resultScreenLeft
					+ (resultScreenWidth * 5 / 7), checkY, checkWidth,
					checkHeight);
		}
	}

	/*
	 * private void drawInstructions() {
	 * 
	 * float top = height / 2.0f + 5.0f; float offset = 10; float arrowSize =
	 * 14;
	 * 
	 * batcher.draw(arrowRightTexture, width / 2 + offset, top + 40, arrowSize /
	 * 2, arrowSize / 2, arrowSize, arrowSize, 1, 1, -7);
	 * 
	 * batcher.draw(arrowLeftTexture, width / 2 - offset - arrowSize, top + 40,
	 * arrowSize / 2, arrowSize / 2, arrowSize, arrowSize, 1, 1, 7);
	 * 
	 * batcher.draw(missileOff, width / 2 - offset - arrowSize -
	 * missile.getWidth(), top + 20, missile.getWidth() / 2.0f,
	 * missile.getHeight() / 2.0f, missile.getWidth(), missile.getHeight(), 1,
	 * 1, 15);
	 * 
	 * batcher.draw(missileOff, width / 2 + offset + 5 + missile.getWidth(), top
	 * + 20, missile.getWidth() / 2.0f, missile.getHeight() / 2.0f,
	 * missile.getWidth(), missile.getHeight(), 1, 1, -15);
	 * 
	 * batcher.draw(missileOff, width / 2 - missile.getWidth() / 2, top - 10,
	 * missile.getWidth() / 2.0f, missile.getHeight() / 2.0f,
	 * missile.getWidth(), missile.getHeight(), 1, 1, 0);
	 * 
	 * batcher.draw(tapRightTexture, width / 2 + offset + 5, top - 2, 21, 25);
	 * 
	 * batcher.draw(tapLeftTexture, width / 2 - offset - 17 - 5, top - 2, 17,
	 * 25);
	 * 
	 * }
	 */
	private void drawInstructions() {

		float top = height / 2.0f + 5.0f;
		float offset = 12;
		float arrowSize = 14;

		batcher.draw(arrowRightTexture, width / 2 + offset, top + 10,
				arrowSize / 2, arrowSize / 2, arrowSize, arrowSize, 1, 1, -7);

		batcher.draw(arrowLeftTexture, width / 2 - offset - arrowSize,
				top + 10, arrowSize / 2, arrowSize / 2, arrowSize, arrowSize,
				1, 1, 7);

		batcher.draw(missileOff,
				width / 2 - offset - arrowSize - missile.getWidth(), top - 10,
				missile.getWidth() / 2.0f, missile.getHeight() / 2.0f,
				missile.getWidth(), missile.getHeight(), 1, 1, 15);

		batcher.draw(missileOff, width / 2 + offset + 5 + missile.getWidth(),
				top - 10, missile.getWidth() / 2.0f,
				missile.getHeight() / 2.0f, missile.getWidth(),
				missile.getHeight(), 1, 1, -15);

		batcher.draw(missileOff, width / 2 - missile.getWidth() / 2, top - 10,
				missile.getWidth() / 2.0f, missile.getHeight() / 2.0f,
				missile.getWidth(), missile.getHeight(), 1, 1, 0);

		batcher.draw(tapRightTexture, width / 2 + offset + 15, top + 45, 21, 25);

		batcher.draw(tapLeftTexture, width / 2 - offset - 17 - 15, top + 45,
				17, 25);

	}

	private void drawShareRetry(SpriteBatch batcher) {
		world.getShareButton().draw(batcher);
		world.getRetryButton().draw(batcher);
	}

	private void drawRatePlay(SpriteBatch batcher) {
		world.getRateButton().draw(batcher);
		world.getPlayButton().draw(batcher);
	}

	private void drawOk(SpriteBatch batcher) {
		world.getOkButton().draw(batcher);
		world.getOkButton().draw(batcher);
	}

	private void drawTitle() {

		AssetLoader.shadow.draw(batcher, "WobbleRocket", 7, 79);
		AssetLoader.font.draw(batcher, "WobbleRocket", 8, 80);
	}

	private void drawMissileDemo(float runtime) {
		loadDemo(world.getUnlocked());
		batcher.draw(missileDemoAnimation.getKeyFrame(runtime),
				missileDemo.getX(), missileDemo.getY(),
				missileDemo.getWidth() / 2.0f, missileDemo.getHeight() / 2.0f,
				missileDemo.getWidth(), missileDemo.getHeight(), 1, 1,
				missileDemo.getRotation());
	}

	public void render(float delta, float runtime) {

		// Draw missile
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Tells shapeRenderer to begin drawing filled shapes
		/*
		 * renderer.begin(ShapeType.Filled); renderer.setColor(skyColour);
		 * renderer.rect(0, 0, width, height);
		 * 
		 * // Draw grey for buildings
		 * 
		 * renderer.end();
		 */
		/*
		 * Draw the bounding box for debugging
		 */
		// renderer.begin(ShapeType.Line);
		// renderer.setColor(237 / 255.0f, 25 / 255.0f, 25 / 255.0f, 1);
		// renderer.polygon(missile.getBounds()); renderer.end();

		batcher.begin();
		batcher.enableBlending();

		batcher.draw(background, 0, 0, width, height, 0, 0, 1, 1);
 
		if (world.isUnlocked()) {

			drawUnlocked();
			drawEdge();
			drawMissileDemo(runtime); // after reset
			drawOk(batcher);
			needsLoading = true;
		} else {
			drawObstacles();

			drawEdge();
			drawMissile(runtime);

			if (world.isGameOver()) {
				drawGameOver();
				drawResults();
				drawShareRetry(batcher);
			} else if (world.isHighScore()) {
				drawHighScore();
				drawResults();
				drawShareRetry(batcher);
				
			} else if (world.isRunning()) {
				if (needsLoading)
				{
					loadRocket(AssetLoader.getHighScore());					
				}
				drawScore();
			} else if (world.isReady()) {				
				loadRocket(AssetLoader.getHighScore());
				drawTitle();
				drawInstructions();
				drawRatePlay(batcher);
			} 
		}
		batcher.end();

	}
}
