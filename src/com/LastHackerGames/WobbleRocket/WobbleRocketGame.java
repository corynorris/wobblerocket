package com.LastHackerGames.WobbleRocket;

import com.LastHackerGames.WobbleRocket.Helpers.AssetLoader;
import com.LastHackerGames.WobbleRocket.Screens.GameScreen;
import com.badlogic.gdx.Game;

public class WobbleRocketGame extends Game {

	private IActivityRequestHandler myRequestHandler;

	public WobbleRocketGame(IActivityRequestHandler handler)
	{
		this.myRequestHandler = handler;
	}


	@Override
	public void create() {
		AssetLoader.load();
		setScreen(new GameScreen(myRequestHandler));
	}

	@Override
	public void dispose() {
		super.dispose();
		AssetLoader.dispose();
	}

}