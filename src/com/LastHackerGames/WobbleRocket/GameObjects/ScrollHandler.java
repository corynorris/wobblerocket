package com.LastHackerGames.WobbleRocket.GameObjects;

import java.util.Random;

import com.LastHackerGames.WobbleRocket.GameWorld.GameWorld;
import com.LastHackerGames.WobbleRocket.Helpers.AssetLoader;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class ScrollHandler {

	private Random r;
	private Wall wall1, wall2;
	private Edge leftEdge1, leftEdge2;
	private Edge rightEdge1, rightEdge2;

	private int lastSplit;

	public static final int LEFT = 0;
	public static final int RIGHT = 136;
	public static final int TOP = 0;
	public static final int BOTTOM = 204;
	public static final Vector2 SCROLL_SPEED = new Vector2(0, -110);
	public static final int GAP = 100; // 204 + 36 / 3
	public static final int WALL_WIDTH = 10;
	public static final int WALL_MIN_HEIGHT = 50;
	public static final int WALL_MIN_GAP = 40;
	public static final int EDGE_WIDTH = 7;

	private GameWorld gameWorld;

	
	public static final int MIN_GAP = 30;
	public int difficulty;
	
	public ScrollHandler(GameWorld gameWorld, float midpoint) {
		this.gameWorld = gameWorld;
		leftEdge1 = new Edge(LEFT, TOP, EDGE_WIDTH, BOTTOM, SCROLL_SPEED);
		leftEdge2 = new Edge(LEFT, leftEdge1.getTail(), EDGE_WIDTH, BOTTOM,
				SCROLL_SPEED);
		rightEdge1 = new Edge(RIGHT - EDGE_WIDTH, TOP, EDGE_WIDTH, BOTTOM,
				SCROLL_SPEED);
		rightEdge2 = new Edge(RIGHT - EDGE_WIDTH, rightEdge1.getTail(),
				EDGE_WIDTH, BOTTOM, SCROLL_SPEED);

		r = new Random();

		difficulty = MIN_GAP + 20;
		
		lastSplit = getRandomBetween(LEFT + WALL_MIN_HEIGHT, RIGHT
				- WALL_MIN_HEIGHT);
		wall1 = new Wall(LEFT, BOTTOM+50, lastSplit, WALL_WIDTH, SCROLL_SPEED);

		lastSplit = getRandomBetween(LEFT + WALL_MIN_HEIGHT, lastSplit);
		wall2 = new Wall(lastSplit, wall1.getY() + GAP, RIGHT, WALL_WIDTH,
				SCROLL_SPEED);
	}

	public int getRandomBetween(int a, int b) {
		if (b-a <= 0)
		{
			System.out.println("b-a");
			return a;

		}
		
		int random = r.nextInt(b - a) + a;
		//System.out.println("("+a+","+b+") : "+random);
		
		return random;
	}

	public void updateReady(float delta) {
		leftEdge1.update(delta);
		leftEdge2.update(delta);
		rightEdge1.update(delta);
		rightEdge2.update(delta);

		if (leftEdge1.isScrolled()) {
			leftEdge1.reset(new Vector2(LEFT, leftEdge2.getTail()));
		} else if (leftEdge2.isScrolled()) {
			leftEdge2.reset(new Vector2(LEFT, leftEdge1.getTail()));
		}

		if (rightEdge1.isScrolled()) {
			rightEdge1.reset(new Vector2(RIGHT - EDGE_WIDTH, rightEdge2
					.getTail()));
		} else if (rightEdge2.isScrolled()) {
			rightEdge2.reset(new Vector2(RIGHT - EDGE_WIDTH, rightEdge1
					.getTail()));
		}

	}

	public void update(float delta) {

		updateReady(delta);

		wall1.update(delta);
		wall2.update(delta);

		if (wall1.isScrolled()) {
			
			lastSplit = getRandomBetween(lastSplit , RIGHT);
		
			lastSplit =MathUtils.clamp(lastSplit, LEFT + EDGE_WIDTH + difficulty, RIGHT - EDGE_WIDTH - difficulty);
			wall1.reset(new Vector2(LEFT, wall2.getTail() + GAP), lastSplit);
		
		} else if (wall2.isScrolled()) {
			
			lastSplit = getRandomBetween(LEFT, lastSplit);			
			lastSplit =MathUtils.clamp(lastSplit, LEFT + EDGE_WIDTH + difficulty, RIGHT - EDGE_WIDTH - difficulty);
			wall2.reset(new Vector2(lastSplit, wall1.getTail() + GAP), RIGHT);
			
			//make it harder every time two walls cycle
			difficulty--;
			if (difficulty < MIN_GAP)
				difficulty = MIN_GAP;
			
		
		}
		
		

	}

	public void stop() {
		wall1.stop();
		wall2.stop();
		leftEdge1.stop();
		leftEdge2.stop();
		rightEdge1.stop();
		rightEdge2.stop();
	}

	public boolean collides(Missile missile) {

		if (!wall1.isScored()
				&& wall1.getY() + (wall1.getHeight() / 1) < missile.getY()
						+ missile.getHeight()) {
			addScore(1);
			wall1.setScored(true);
			AssetLoader.scoreSound.play();
		} else if (!wall2.isScored()
				&& wall2.getY() + (wall2.getHeight() / 1) < missile.getY()
						+ missile.getHeight()) {
			addScore(1);
			wall2.setScored(true);
			AssetLoader.scoreSound.play();

		}

		return (wall1.collides(missile) || wall2.collides(missile)
				|| rightEdge1.collides(missile) || rightEdge2.collides(missile)
				|| leftEdge1.collides(missile) || leftEdge2.collides(missile));
	}

	private void addScore(int increment) {
		gameWorld.addScore(increment);
	}

	public void onRestart() {
		wall1.onRestart();
		wall2.onRestart();
		leftEdge1.onRestart();
		leftEdge2.onRestart();
		rightEdge1.onRestart();
		rightEdge2.onRestart();
		difficulty = MIN_GAP + 25;
	}

	public Wall getwall1() {
		return wall1;
	}

	public Wall getwall2() {
		return wall2;
	}

	public Edge getBottomEdge1() {
		// TODO Auto-generated method stub
		return leftEdge1;
	}

	public Edge getBottomEdge2() {
		// TODO Auto-generated method stub
		return leftEdge2;
	}

	public Edge getTopEdge1() {
		// TODO Auto-generated method stub
		return rightEdge1;
	}

	public Edge getTopEdge2() {
		// TODO Auto-generated method stub
		return rightEdge2;
	}
}