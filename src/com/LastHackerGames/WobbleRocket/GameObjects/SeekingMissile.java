package com.LastHackerGames.WobbleRocket.GameObjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class SeekingMissile extends Missile {
	
	private Vector2 goal;	
	
	protected final float SNAP_DIST = 10.0f;
	protected final float MAX_VELOCITY = 170.0f;
	protected final float MAX_ACCEL = 350.0f;
	protected final float MAX_ROTATION = 30.0f;
	
	
	public SeekingMissile(float x, float y, int width, int height) {
		super(x, y, width, height);
		this.goal = new Vector2(x, y);
		// TODO Auto-generated constructor stub
	}

	public void onRestart()
	{
		super.onRestart();
		goal =  origPosition.cpy();
	}
	public void update(float delta) {
		// Keep the missile on and at a reasonable speed.
		if (isAlive()) {
			float x = (goal.y - position.y) / dist;
			accel.y = MathUtils
					.clamp(-1200.0f * x + 600, -MAX_ACCEL, MAX_ACCEL);

			Vector2 nextPos = position.cpy().add(velocity.cpy().scl(delta));

			if (between(goal.y, position.y, nextPos.y)
					&& between(velocity.y, -SNAP_DIST, SNAP_DIST)) {
				position.y = goal.y;
				velocity.y = 0;
				rotation = 0;
			} else {

				if (between(goal.y, position.y, nextPos.y)
						&& !between(velocity.y, -SNAP_DIST, SNAP_DIST)) {
					velocity.scl(0.7f);
					dist = goal.y - position.y;

				}

				if (dist > 0) // Travelling down
				{
					velocity.sub(accel.cpy().scl(delta));
				} else if (dist < 0) // Travelling up
				{
					velocity.add(accel.cpy().scl(delta));
				}

				rotation = velocity.y / MAX_VELOCITY * MAX_ROTATION;

				velocity.y = MathUtils.clamp(velocity.y, -MAX_VELOCITY,
						MAX_VELOCITY);

			}

		} else {
			velocity.y += 200 * delta;
		}
		
		position.add(velocity.cpy().scl(delta));
		position.y = MathUtils.clamp(position.y, 0, 204 - height);

	}


	public Vector2 getGoal() {
		return this.goal;
	}

	@Override
	public void touchDown(int screenX, int screenY) {		
		goal.set(screenX, screenY - (height / 2));
		dist = goal.y - position.y;
	}
}
