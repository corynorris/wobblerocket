package com.LastHackerGames.WobbleRocket.GameObjects;

import com.badlogic.gdx.math.Vector2;

public abstract class Scrollable {

	// Protected is similar to private, but allows inheritance by subclasses.
	protected Vector2 position, origPosition;
	protected Vector2 velocity, origVelocity;

	protected boolean isScrolled;

	public Scrollable(float x, float y, Vector2 velocity) {
		this.position = new Vector2(x, y);
		this.origPosition = position.cpy();
		this.velocity = velocity.cpy();
		this.origVelocity = velocity.cpy();
		this.isScrolled = false;
	}

	// Reset: Should Override in subclass for more specific behavior
	protected abstract boolean scrolledOutOfRange();
	
	public abstract float getTail();

	public void update(float delta) {
		position.add(velocity.cpy().scl(delta));

		// If the Scrollable object is no longer visible:
		if (scrolledOutOfRange()) {
			isScrolled = true;
		}
	}

	public void onRestart()
	{
		position.set(origPosition.cpy());
		velocity.set(origVelocity.cpy());
		this.isScrolled = false;
	}
	
	public void reset(Vector2 newPosition)
	{
		this.position = newPosition.cpy();
		this.isScrolled = false;
	}
	public void stop() {
		velocity.set(0, 0);
	}

	// Getters for instance variables
	public boolean isScrolled() {
		return isScrolled;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}


}
