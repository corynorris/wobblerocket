package com.LastHackerGames.WobbleRocket.GameObjects;

import com.badlogic.gdx.math.MathUtils;

public class WobblyMissile extends Missile {

	private final float MAX_VELOCITY = 160.0f;
	protected final float MAX_ACCEL = 330.0f;
	protected final float MAX_ROTATION = 40.0f;

	public WobblyMissile(float x, float y, int width, int height) {
		super(x, y, width, height);

	}

	public void touchDown(int screenX, int screenY) {
		accel.x = Math.signum(screenX - position.x) * MAX_ACCEL;

	}

	@Override
	public void update(float delta) {

		// Accelerate in the given direction
		velocity.add(accel.cpy().scl(delta));

		velocity.x = MathUtils.clamp(velocity.x, -MAX_VELOCITY, MAX_VELOCITY);
		rotation = -velocity.x / MAX_VELOCITY * MAX_ROTATION;

		position.add(velocity.cpy().scl(delta));
		position.x = MathUtils.clamp(position.x, 0, 204 - height);

	}

}
