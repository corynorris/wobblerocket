package com.LastHackerGames.WobbleRocket.GameObjects;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public class Wall extends Scrollable {
	
	private Polygon bounds;

	int width = 0;
	int height = 0;
	
	boolean isScored;

	
	public Wall(float x, float y, int width, int height, Vector2 scrollSpeed) {
		super(x, y, scrollSpeed);
		this.width = width;
		this.height = height;
		this.isScored = false;
		
		this.bounds = new Polygon(new float[] { 0, 0 , width,
				0 , width, height , 0, height });
		this.bounds.setOrigin(width  / 2, height / 2);

	}
	

	public void onRestart()
	{
		super.onRestart();
		isScored = false;
	}
	
	
	public void reset(Vector2 newPosition, int width )
	{
		this.isScored = false;
		super.reset(newPosition);
		this.width = width;
		this.bounds = new Polygon(new float[] { 0, 0 , width,
				0 , width, height , 0, height });
		this.bounds.setOrigin(width  / 2, height / 2);

	}
	
	public boolean collides(Missile missile)
	{
		if (position.y < missile.getY() + missile.getHeight()) {
			return (Intersector.overlapConvexPolygons(missile.getBounds(), this.getBounds(), null));
		}
		return false;
		
	}
	
	public void update(float delta) {
		super.update(delta);
		bounds.setPosition(position.x, position.y);
	}
	
	
	protected boolean scrolledOutOfRange() {
		return (position.y + height < 0);
	}

	@Override
	public float getTail() {
		return height + this.position.y;

	}

	public float getWidth()
	{
		return width;
	}
	
	public float getHeight()
	{
		return height;
	}
	public float[] getBounds()
	{
		return bounds.getTransformedVertices();
	}
	
	public boolean isScored()
	{
		return isScored;
	}
	
	public void setScored(boolean scored)
	{
		this.isScored = scored;
	}
	
}