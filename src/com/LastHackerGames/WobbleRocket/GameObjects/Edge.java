package com.LastHackerGames.WobbleRocket.GameObjects;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public class Edge extends Scrollable {

	private int width;
	private int height;

	private Polygon bounds;

	public Edge(float x, float y, int width, int height,
			Vector2 scrollSpeed) {
		super(x, y, scrollSpeed);

		this.width = width;
		this.height = height;

		this.bounds = new Polygon(new float[] { 0, 0, width, 0, width, height,
				0, height });
		this.bounds.setOrigin(width / 2, height / 2);
	}

	public void update(float delta) {
		super.update(delta);
		bounds.setPosition(position.x, position.y);
	}

	public float[] getBounds() {
		return bounds.getTransformedVertices();
	}

	public void reset(Vector2 newPosition) {
		super.reset(newPosition);
		this.bounds = new Polygon(new float[] { 0, 0, width, 0, width, height,
				0, height });
		this.bounds.setOrigin(width / 2, height / 2);

	}

	public boolean collides(Missile missile) {

		return (Intersector.overlapConvexPolygons(missile.getBounds(),
				this.getBounds(), null));
	}

	@Override
	protected boolean scrolledOutOfRange() {
		return (position.y + height < 0);
	}

	@Override
	public float getTail() {
		return this.height + this.position.y;

	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
