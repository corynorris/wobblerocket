package com.LastHackerGames.WobbleRocket.GameObjects;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public abstract class Missile {

	protected Vector2 position, velocity, accel, origPosition;

	private float width;
	protected float height;
	protected float rotation;
	protected float dist;
	private Polygon bounds;

	boolean isAlive;

	public Missile(float x, float y, int width, int height) {
		this.position = new Vector2(x, y);
		this.origPosition = new Vector2(x, y);

		this.velocity = new Vector2(0.0f, 0.0f);
		this.accel = new Vector2(0.0f, 0.0f);

		this.width = width;
		this.height = height;
		this.dist = 0;
		this.rotation = 0;
		this.isAlive = true;

		// Create the bounding box
		float x_offset = 1.5f;
		float fire_size = 7.0f;

		this.bounds = new Polygon(new float[] { 
				0 + x_offset, fire_size,
				0 + x_offset, height - 2, 
				// TIP
				0 + x_offset+2, height,
				width - x_offset-2, height, 
				
				width - x_offset, height - 2, 	
				width - x_offset, fire_size
				
				});
		this.bounds.setOrigin((width - (2 * x_offset)) / 2, height / 2);

	}

	public void updateReady(float runTime) {
		velocity.x =  (float) ((float) -1.00*Math.sin(5 * runTime));
		position.add(velocity.cpy());// = offset + origPosition.x;
			rotation =-velocity.x*10;
		velocity.x = 0;
	}
	

	public void onRestart() {
		position = origPosition.cpy();
		accel.set(0, 0);
		velocity.set(0, 0);
		isAlive = true;
	}

	public float[] getBounds() {
		bounds.setRotation(rotation);
		bounds.setPosition(position.x, position.y);
		return this.bounds.getTransformedVertices();
	}

	protected boolean between(float a, float b, float c) {
		return ((a <= b && a >= c) || (a >= b && a <= c));
	}

	public abstract void update(float delta);

	public float getX() {
		// TODO Auto-generated method stub
		return position.x;
	}

	public float getY() {
		// TODO Auto-generated method stub
		return position.y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getRotation() {
		return rotation;
	}

	public abstract void touchDown(int screenX, int screenY);

	public void die() {
		isAlive = false;

	}

	public boolean isAlive() {
		// TODO Auto-generated method stub
		return isAlive;
	}
}
