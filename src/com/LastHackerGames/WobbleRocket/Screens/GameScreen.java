package com.LastHackerGames.WobbleRocket.Screens;

import com.LastHackerGames.WobbleRocket.IActivityRequestHandler;
import com.LastHackerGames.WobbleRocket.GameWorld.GameRenderer;
import com.LastHackerGames.WobbleRocket.GameWorld.GameWorld;
import com.LastHackerGames.WobbleRocket.Helpers.InputHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class GameScreen implements Screen {

	private GameWorld world;
	private GameRenderer renderer;
	private float runTime;

	public static final float gameWidth = 136.0f;
	public static final float gameHeight = 204.0f;

	 private IActivityRequestHandler myRequestHandler;
	 
	public GameScreen(IActivityRequestHandler myRequestHandler) {
		runTime = 0;
		float width = Gdx.graphics.getWidth();
		float height = Gdx.graphics.getHeight();
		this.myRequestHandler = myRequestHandler;
		int midpoint = (int) (gameHeight / 2);


		world = new GameWorld(midpoint, gameWidth, gameHeight, myRequestHandler);
		Gdx.input.setInputProcessor(new InputHandler(world, width / gameWidth,
				height / gameHeight));
		renderer = new GameRenderer(this.world, gameWidth, gameHeight);
	}

	@Override
	public void render(float delta) {
		runTime += delta;
		world.update(delta);
		renderer.render(delta, runTime);

	}

	@Override
	public void resize(int width, int height) {
		System.out.println(width + ", " + height);

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
