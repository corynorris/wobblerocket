package com.LastHackerGames.WobbleRocket.Helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetLoader {

	public static Texture texture, brick, background;
	public static Animation missileAnimation, missileAnimationGreen,
			missileAnimationOrange, missileAnimationRed, missileAnimationGold,
			missileAnimationRainbow;
	public static Sound scoreSound, deadSound, unlockedSound, highscoreSound,
			buttonPressedSound, gameOverSound;
	public static TextureRegion missile1, missile2, missile3, missileOff,
			missileDead, missileGreen1, missileGreen2, missileGreen3,
			missileOrange1, missileOrange2, missileOrange3, missileGold1,
			missileGold2, missileGold3, missileRed1, missileRed2, missileRed3,
			missileRainbow1, missileRainbow2, missileRainbow3,
			missileGreenDead, missileOrangeDead, missileGoldDead,
			missileRedDead, missileRainbowDead;
	public static TextureRegion bottomEdge, topEdge;
	public static TextureRegion brickLeftEnd, brickRightEnd;
	public static TextureRegion resultScreen, checkOn, checkOff;
	public static TextureRegion arrowLeft, arrowRight, tapLeft, tapRight;
	public static TextureRegion gameOver, highScore, unlocked;
	public static TextureRegion play, playPressed, retry, retryPressed, rate,
			ratePressed, share, sharePressed, ok, okPressed;

	public static BitmapFont font, microFont, shadow;
	private static Preferences prefs;

	public static void load() {
		texture = new Texture(Gdx.files.internal("data/rocketicons.png"));
		texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		// texture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);

		brick = new Texture(Gdx.files.internal("data/brick.png"));
		brick.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		brick.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);

		background = new Texture(Gdx.files.internal("data/background.png"));
		background.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

		highScore = new TextureRegion(texture, 45, 620, 184, 33);
		highScore.flip(false, true);

		gameOver = new TextureRegion(texture, 45, 656, 180, 33);
		gameOver.flip(false, true);

		unlocked = new TextureRegion(texture, 45, 692, 174, 33);
		unlocked.flip(false, true);

		// 13x8
		rate = new TextureRegion(texture, 48, 728, 83, 31);
		rate.flip(false, true);
		ratePressed = new TextureRegion(texture, 142, 728, 83, 31);
		ratePressed.flip(false, true);

		// 15x8
		play = new TextureRegion(texture, 48, 764, 83, 31);
		play.flip(false, true);
		playPressed = new TextureRegion(texture, 142, 764, 83, 31);
		playPressed.flip(false, true);

		// 22x8
		retry = new TextureRegion(texture, 48, 801, 96, 31);
		retry.flip(false, true);
		retryPressed = new TextureRegion(texture, 152, 801, 96, 31);
		retryPressed.flip(false, true);

		// 18x8
		share = new TextureRegion(texture, 238, 731, 98, 31);
		share.flip(false, true);
		sharePressed = new TextureRegion(texture, 238, 765, 98, 31);
		sharePressed.flip(false, true);

		// 13x8
		ok = new TextureRegion(texture, 232, 621, 60, 31);
		ok.flip(false, true);
		okPressed = new TextureRegion(texture, 232, 657, 60, 31);
		okPressed.flip(false, true);

		tapLeft = new TextureRegion(texture, 4, 921, 68, 102);
		tapLeft.flip(false, true);

		tapRight = new TextureRegion(texture, 72, 921, 84, 102);
		tapRight.flip(false, true);

		bottomEdge = new TextureRegion(texture, 0, 0, 36, 827);
		topEdge = new TextureRegion(texture, 0, 0, 36, 827);
		bottomEdge.flip(false, true);
		topEdge.flip(true, false);

		// 11 x 35
		missile1 = new TextureRegion(texture, 36, 0, 40, 185);
		missile1.flip(true, true);

		missile2 = new TextureRegion(texture, 76, 0, 40, 185);
		missile2.flip(true, true);

		missile3 = new TextureRegion(texture, 116, 0, 40, 185);
		missile3.flip(true, true);

		missileDead = new TextureRegion(texture, 156, 0, 40, 185);
		missileDead.flip(true, true);

		missileOff = new TextureRegion(texture, 196, 0, 40, 185);
		missileOff.flip(true, true);

		// Orange 11 x 35
		missileOrange1 = new TextureRegion(texture, 352, 0, 40, 185);
		missileOrange1.flip(true, true);

		missileOrange2 = new TextureRegion(texture, 392, 0, 40, 185);
		missileOrange2.flip(true, true);

		missileOrange3 = new TextureRegion(texture, 432, 0, 40, 185);
		missileOrange3.flip(true, true);

		missileOrangeDead = new TextureRegion(texture, 472, 0, 40, 185);
		missileOrangeDead.flip(true, true);

		// Gold 11 x 35
		missileGold1 = new TextureRegion(texture, 352, 188, 40, 185);
		missileGold1.flip(true, true);

		missileGold2 = new TextureRegion(texture, 392, 188, 40, 185);
		missileGold2.flip(true, true);

		missileGold3 = new TextureRegion(texture, 432, 188, 40, 185);
		missileGold3.flip(true, true);

		missileGoldDead = new TextureRegion(texture, 472, 188, 40, 185);
		missileGoldDead.flip(true, true);

		// Green 11 x 35
		missileGreen1 = new TextureRegion(texture, 352, 377, 40, 185);
		missileGreen1.flip(true, true);

		missileGreen2 = new TextureRegion(texture, 392, 377, 40, 185);
		missileGreen2.flip(true, true);

		missileGreen3 = new TextureRegion(texture, 432, 377, 40, 185);
		missileGreen3.flip(true, true);

		missileGreenDead = new TextureRegion(texture, 472, 377, 40, 185);
		missileGreenDead.flip(true, true);

		// Red 11 x 35
		missileRed1 = new TextureRegion(texture, 352, 567, 40, 185);
		missileRed1.flip(true, true);

		missileRed2 = new TextureRegion(texture, 392, 567, 40, 185);
		missileRed2.flip(true, true);

		missileRed3 = new TextureRegion(texture, 432, 567, 40, 185);
		missileRed3.flip(true, true);

		missileRedDead = new TextureRegion(texture, 472, 567, 40, 185);
		missileRedDead.flip(true, true);

		// Rainbow 11 x 35
		missileRainbow1 = new TextureRegion(texture, 352, 753, 40, 185);
		missileRainbow1.flip(true, true);

		missileRainbow2 = new TextureRegion(texture, 392, 753, 40, 185);
		missileRainbow2.flip(true, true);

		missileRainbow3 = new TextureRegion(texture, 432, 753, 40, 185);
		missileRainbow3.flip(true, true);

		missileRainbowDead = new TextureRegion(texture, 472, 753, 40, 185);
		missileRainbowDead.flip(true, true);

		TextureRegion[] missiles = { missile1, missile2, missile3 };

		missileAnimation = new Animation(0.10f, missiles);
		missileAnimation.setPlayMode(Animation.LOOP_PINGPONG);

		TextureRegion[] greenMissiles = { missileGreen1, missileGreen2,
				missileGreen3 };

		missileAnimationGreen = new Animation(0.10f, greenMissiles);
		missileAnimationGreen.setPlayMode(Animation.LOOP_PINGPONG);

		TextureRegion[] orangeMissiles = { missileOrange1, missileOrange2,
				missileOrange3 };

		missileAnimationOrange = new Animation(0.10f, orangeMissiles);
		missileAnimationOrange.setPlayMode(Animation.LOOP_PINGPONG);

		TextureRegion[] goldMissiles = { missileGold1, missileGold2,
				missileGold3 };

		missileAnimationGold = new Animation(0.10f, goldMissiles);
		missileAnimationGold.setPlayMode(Animation.LOOP_PINGPONG);

		TextureRegion[] redMissiles = { missileRed1, missileRed2, missileRed3 };

		missileAnimationRed = new Animation(0.10f, redMissiles);
		missileAnimationRed.setPlayMode(Animation.LOOP_PINGPONG);

		TextureRegion[] rainbowMissiles = { missileRainbow1, missileRainbow2,
				missileRainbow3 };

		missileAnimationRainbow = new Animation(0.10f, rainbowMissiles);
		missileAnimationRainbow.setPlayMode(Animation.LOOP_PINGPONG);

		resultScreen = new TextureRegion(texture, 37, 252, 298, 207);
		resultScreen.flip(false, true);

		checkOn = new TextureRegion(texture, 36, 468, 46, 74);
		checkOn.flip(false, false);

		checkOff = new TextureRegion(texture, 83, 468, 46, 74);
		checkOff.flip(false, false);

		arrowLeft = new TextureRegion(texture, 56, 186, 38, 40);
		arrowLeft.flip(false, true);

		arrowRight = new TextureRegion(texture, 94, 186, 38, 40);
		arrowRight.flip(false, true);

		brickLeftEnd = new TextureRegion(texture, 36, 186, 20, 64);
		brickLeftEnd.flip(false, true);
		brickRightEnd = new TextureRegion(texture, 36, 186, 20, 64);
		brickRightEnd.flip(true, true);

		scoreSound = Gdx.audio.newSound(Gdx.files.internal("data/success.wav"));
		deadSound = Gdx.audio.newSound(Gdx.files.internal("data/dead.wav"));
		unlockedSound = Gdx.audio.newSound(Gdx.files
				.internal("data/unlocked.mp3"));
		gameOverSound = Gdx.audio.newSound(Gdx.files
				.internal("data/game-over.wav"));
		highscoreSound = Gdx.audio.newSound(Gdx.files
				.internal("data/highscore.mp3"));
		buttonPressedSound = Gdx.audio.newSound(Gdx.files
				.internal("data/buttonPressed.mp3"));
		// Create (or retrieve existing) preferences file
		prefs = Gdx.app.getPreferences("WobbleRocket");

		if (!prefs.contains("highScore")) {
			prefs.putInteger("highScore", 0);
		}

		font = new BitmapFont(Gdx.files.internal("data/text.fnt"));
		font.setScale(.25f, -.25f);
		shadow = new BitmapFont(Gdx.files.internal("data/shadow.fnt"));
		shadow.setScale(.25f, -.25f);

		microFont = new BitmapFont(Gdx.files.internal("data/text.fnt"));
		microFont.setScale(.12f, -.12f);

		// Clear Prefs
		//prefs.putInteger("highScore",0);
		//prefs.flush();
	}

	public static void setHighScore(int val) {
		prefs.putInteger("highScore", val);
		prefs.flush();
	}

	public static int getHighScore() {
		return prefs.getInteger("highScore");
	}

	public static void dispose() {
		// We must dispose of the texture when we are finished.
		texture.dispose();
		scoreSound.dispose();
		deadSound.dispose();
		unlockedSound.dispose();
		highscoreSound.dispose();
		buttonPressedSound.dispose();
		gameOverSound.dispose();

		brick.dispose();

		background.dispose();

		font.dispose();
		shadow.dispose();

		microFont.dispose();
	}
}
