package com.LastHackerGames.WobbleRocket.Helpers;

import com.LastHackerGames.WobbleRocket.GameObjects.Missile;
import com.LastHackerGames.WobbleRocket.GameWorld.GameWorld;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

public class InputHandler implements InputProcessor {

	private GameWorld world;
	private Missile missile;
	private float scaleFactorX;
	private float scaleFactorY;

	public InputHandler(GameWorld world, float scaleFactorX, float scaleFactorY) {
		this.world = world;
		this.missile = world.getMissile();

		this.scaleFactorX = scaleFactorX;
		this.scaleFactorY = scaleFactorY;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		screenX = scaleX(screenX);
		screenY = scaleY(screenY);

		if (world.isReady()) {
			world.getPlayButton().isTouchDown(screenX, screenY);
			world.getRateButton().isTouchDown(screenX, screenY);
		} else if (world.isGameUnlocked()) {
			world.getOkButton().isTouchDown(screenX, screenY);
		} else if (world.isGameOver() || world.isHighScore()) {
			world.getShareButton().isTouchDown(screenX, screenY);
			world.getRetryButton().isTouchDown(screenX, screenY);
		} else {
			missile.touchDown(screenX, screenY);
		}
		return true;

	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		/*
		 * screenX = scaleX(screenX); screenY = scaleY(screenY);
		 * missile.touchDown(screenX, screenY);
		 */
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	private int scaleX(int screenX) {
		return (int) (screenX / scaleFactorX);
	}

	private int scaleY(int screenY) {
		return (int) (screenY / scaleFactorY);
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		screenX = scaleX(screenX);
		screenY = scaleY(screenY);
		if (world.isReady()) {
			if (world.getPlayButton().isTouchUp(screenX, screenY)) {
				world.start();
				return true;
			} else if (world.getRateButton().isTouchUp(screenX, screenY)) {
				Gdx.net.openURI("https://play.google.com/store/apps/details?id=com.LastHackerGames.WobbleRocket");
				return true;
			}
		} else if (world.isGameUnlocked()) {
			if (world.getOkButton().isTouchUp(screenX, screenY)) {
				world.nextUnlock();
				if (world.isUnlocked())
				{
					world.unlocked();
				} else 
				{
					world.highscore();
				}
				return true;
			}
		} else if (world.isGameOver() || world.isHighScore()) {
			if (world.getRetryButton().isTouchUp(screenX, screenY)) {
				world.restart();
				return true;
			} else if (world.getShareButton().isTouchUp(screenX, screenY)) {
				Gdx.net.openURI("https://twitter.com/share?url=https://play.google.com/store/apps/details?id=com.LastHackerGames.WobbleRocket&text=Just scored: "+ world.getScore()+" on WobbleRocket!");
				return true;
			}
		}

		return false;
	}

}
